<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInitialTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('countries', function (Blueprint $table) {
            $table->string('code')->index();
            $table->string('name');
        });
        
        Schema::create('queries', function (Blueprint $table){
            $table->increments('query_id');
            $table->string('city');
            $table->string('country');
            $table->string('json_result');
            $table->timestamp('created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('countries');
        Schema::drop('query_history');
    }
}
