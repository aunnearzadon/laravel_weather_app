<?php

use Illuminate\Database\Seeder;

class CountryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('countries')->insert([
            [
                'code' => 'GB',
                'name' => 'United Kingdom'
            ],
            [
                'code' => 'US',
                'name' => 'United States of America'
            ],
            [
                'code' => 'PH',
                'name' => 'Philippines'
            ],
            [
                'code' => 'PL',
                'name' => 'Poland'
            ],
            [
                'code' => 'DE',
                'name' => 'Germany'
            ],
            [
                'code' => 'AU',
                'name' => 'Australia'
            ],
            [
                'code' => 'JP',
                'name' => 'Japan'
            ],
            [
                'code' => 'IN',
                'name' => 'India'
            ],
            [
                'code' => 'BR',
                'name' => 'Brazil'
            ]
        ]);
    }
}
