<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Query;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class WeatherController extends Controller
{
    public function index(){
        $countries = DB::table('countries')->get();        
        return view('weather')->with('countries', $countries);
    }
    
    public function saveQuery(){
        $data = Input::all();
        
        $query = new Query;
        $query->city = $data['city'];
        $query->country = $data['country'];
        $query->json_result = $data['result'];
        $query->save();
        
        return json_encode($query);
    }
}
