<html>
    <head>
        <title>Weather Forcasting App</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">
        
        <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
        
        <meta name="csrf-token" content="{{ csrf_token() }}">
    </head>
    <body>
        <header class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <h1>Laravel Weather Forecasting Application</h1>
                </div>                
            </div>
        </header>
        <section class="main container">
            <div class="row">
                <div class="col-4 offset-4 text-center">
                    <form id="weatherForm">
                        <fieldset>
                            <input class="form-control mb-2" type="text" name="city" placeholder="City"/>
                            <select class="form-control mb-2" name="country">
                                @foreach($countries as $country)
                                <option value="{{ $country->code }}">{{ $country->name }}</option>
                                @endforeach
                            </select>
                            <button class="form-control btn btn-primary mb-2" type="submit">Check Weather</button>
                        </fieldset>
                        <h3 id="status"></h3>
                        <code id="result"></code>
                    </form>
                    <script src="{{ secure_asset('js/weather.js') }}"></script>
                </div>
            </div>
        </section>
        <footer>
            
        </footer>
    </body>
</html>