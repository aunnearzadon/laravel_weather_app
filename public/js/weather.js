$(document).on('submit', '#weatherForm', function(e){
    e.preventDefault();
    var APPID = "4e2ec8fb082f65bfdc2fad9e81256fff";
    var city = $("input[name=city]").val();
    var country = $("select[name=country]").val();
    
    $.get('https://api.openweathermap.org/data/2.5/weather?q=' + city + ',' + country + '&appid=' + APPID, function(res){
        var result = JSON.stringify(res);
        data = {
            city: city,
            country: country,
            result: result
        };
        
        $.ajax({
            url: '/save',
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            method: 'POST',
            data: data,
            dataType: 'json',
            success: function(res){
                $("#status").text('Data Saved');
                $('#status').addClass("alert")
                    .removeClass("alert-danger")
                    .addClass("alert-success");
                $("#result").text(result);
            },
            error: function(res){
                $("#status").text('Error saving the result');
                $("#status").addClass("alert")
                    .addClass("alert-danger")
                    .removeClass("alert-success");
                $("#result").text('');
            }
        });
    }).fail(function(){
        $("#status").text('Error getting the forecast');
        $("#status").addClass("alert")
            .addClass("alert-danger")
            .removeClass("alert-success");
        $("#result").text('');
    });
});